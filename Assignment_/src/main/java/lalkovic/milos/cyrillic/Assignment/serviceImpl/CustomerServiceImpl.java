package lalkovic.milos.cyrillic.Assignment.serviceImpl;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import lalkovic.milos.cyrillic.Assignment.entity.Customer;
import lalkovic.milos.cyrillic.Assignment.service.CustomerService;

@Repository
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	private EntityManager entityManager;

	@Override
	public List<Customer> findAll() {
		Session session = entityManager.unwrap(Session.class);
		Query<Customer> theQuery = session.createQuery("from Customer", Customer.class);
		List<Customer> customers = theQuery.getResultList();

		return customers;
	}

}
