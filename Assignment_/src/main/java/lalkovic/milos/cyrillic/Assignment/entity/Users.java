package lalkovic.milos.cyrillic.Assignment.entity;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "users")
public class Users {

	@Id
	@Column(name = "username")
	private String username;

	@Column(name = "password")
	private String password;

	@JsonIgnore
	@OneToMany(targetEntity = Account.class, mappedBy = "users", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<Account> account = new ArrayList<>();

	public Users() {

	}

	public Users(String username, String password, List<Account> account) {
		this.username = username;
		this.password = password;
		this.account = account;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public List<Account> getAccount() {
		return account;
	}

	public void setAccount(ArrayList<Account> account) {
		this.account = account;
	}

	@Override
	public String toString() {
		return "User [username=" + username + ", password=" + password + ", account=" + account + "]";
	}

}
