package lalkovic.milos.cyrillic.Assignment.service;

import java.util.List;

import lalkovic.milos.cyrillic.Assignment.entity.Customer;

public interface CustomerService {
	public List<Customer> findAll();
}
