package lalkovic.milos.cyrillic.Assignment.service;

import java.util.List;

import lalkovic.milos.cyrillic.Assignment.dto.AccountDTO;
import lalkovic.milos.cyrillic.Assignment.dto.FarmsDTO;

public interface AccountService {
	public List<AccountDTO> findAllAccounts();
	public List<FarmsDTO> findAllFarms();
}
