package lalkovic.milos.cyrillic.Assignment.service;

import java.util.List;

import lalkovic.milos.cyrillic.Assignment.entity.Users;

public interface UserService {
	public List<Users> findAll();
}
