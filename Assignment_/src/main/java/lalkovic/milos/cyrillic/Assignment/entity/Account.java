package lalkovic.milos.cyrillic.Assignment.entity;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "account")
public class Account {

	@Id
	@GeneratedValue
	@Column(name = "id")
	private int id;

	@OneToMany(mappedBy = "account", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private Set<Farm> farm;

	@Column(name = "type")
	private String type;

	@ManyToOne
	@JoinColumn(name = "users_username", referencedColumnName = "username", insertable = false, updatable = true)
	private Users users;

	@OneToOne(mappedBy = "account")
	private Customer cutomer;

	public Account() {
	}

	public Account(Set<Farm> farm, String type, Users users, Customer cutomer) {
		this.farm = farm;
		this.type = type;
		this.users = users;
		this.cutomer = cutomer;
	}

	public Set<Farm> getFarm() {
		return farm;
	}

	public void setFarm(Set<Farm> farm) {
		this.farm = farm;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Users getUser() {
		return users;
	}

	public void setUser(Users users) {
		this.users = users;
	}

	public Customer getCutomer() {
		return cutomer;
	}

	public void setCutomer(Customer cutomer) {
		this.cutomer = cutomer;
	}

	@Override
	public String toString() {
		return "Account [id=" + id + ", farm=" + farm + ", type=" + type + ", users=" + users + ", cutomer=" + cutomer
				+ "]";
	}

}
