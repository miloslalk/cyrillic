package lalkovic.milos.cyrillic.Assignment.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "farm")
public class Farm {

	@Id
	@GeneratedValue
	@Column(name = "id")
	private int id;

	@Column(name = "name")
	private String name;

	@Column(name = "city")
	private String city;

	@Column(name = "size_in_are")
	private Integer sizeInAre;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name = "account_id", nullable = false, unique = true)
	private Account account;

	public Farm() {

	}

	public Farm(String name, String city, Integer sizeInAre, Account account) {
		this.name = name;
		this.city = city;
		this.sizeInAre = sizeInAre;
		this.account = account;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Integer getSizeInAre() {
		return sizeInAre;
	}

	public void setSizeInAre(Integer sizeInAre) {
		this.sizeInAre = sizeInAre;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	@Override
	public String toString() {
		return "Farm [id=" + id + ", name=" + name + ", city=" + city + ", sizeInAre=" + sizeInAre + ", account="
				+ account + "]";
	}

}
