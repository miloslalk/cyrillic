package lalkovic.milos.cyrillic.Assignment.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lalkovic.milos.cyrillic.Assignment.entity.Users;
import lalkovic.milos.cyrillic.Assignment.service.UserService;

@RestController
@RequestMapping("/api")
public class UsersController {

	@Autowired
	private UserService userService;

	@GetMapping("/users")
	public List<Users> findAll() {
		return userService.findAll();
	}

}
