package lalkovic.milos.cyrillic.Assignment.dto;

public class FarmsDTO {

	private String name;
	private String city;
	private String sizeInAre;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getSizeInAre() {
		return sizeInAre;
	}

	public void setSizeInAre(String sizeInAre) {
		this.sizeInAre = sizeInAre;
	}

}
