package lalkovic.milos.cyrillic.Assignment.serviceImpl;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import lalkovic.milos.cyrillic.Assignment.entity.Users;
import lalkovic.milos.cyrillic.Assignment.service.UserService;

@Repository
public class UserServiceImpl implements UserService {

	@Autowired
	private EntityManager entityManager;

	@Override
	public List<Users> findAll() {
		Session session = entityManager.unwrap(Session.class);
		Query<Users> theQuery = session.createQuery("from Users", Users.class);
		List<Users> farms = theQuery.getResultList();
		return farms;
	}

}
