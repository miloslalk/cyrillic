package lalkovic.milos.cyrillic.Assignment.serviceImpl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Repository;

import lalkovic.milos.cyrillic.Assignment.dto.AccountDTO;
import lalkovic.milos.cyrillic.Assignment.dto.FarmsDTO;
import lalkovic.milos.cyrillic.Assignment.entity.Account;
import lalkovic.milos.cyrillic.Assignment.entity.Farm;
import lalkovic.milos.cyrillic.Assignment.service.AccountService;

@Repository
public class AccountServiceImpl implements AccountService {

	@Autowired
	private EntityManager entityManager;

	@Override
	public List<AccountDTO> findAllAccounts() {
		AccountDTO dto;
		List<AccountDTO> accList = new ArrayList<>();
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String currentPrincipalName = authentication.getName();
		Session session = entityManager.unwrap(Session.class);

		Query<Account> theQuery = session.createQuery("from Account where users_username=:user", Account.class)
				.setParameter("user", currentPrincipalName);
		List<Account> accounts = theQuery.getResultList();
		for (Account acc : accounts) {
			dto = new AccountDTO();
			dto.setType(acc.getType());
			accList.add(dto);
		}

		return accList;
	}

	@Override
	public List<FarmsDTO> findAllFarms() {
		FarmsDTO farmsDTO;
		List<FarmsDTO> farmsList = new ArrayList<>();
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String currentPrincipalName = authentication.getName();
		System.out.println(currentPrincipalName);
		Session session = entityManager.unwrap(Session.class);

		Query<Account> theQuery = session.createQuery("from Account where users_username=:user", Account.class)
				.setParameter("user", currentPrincipalName);
		List<Account> accounts = theQuery.getResultList();
		for (Account acc : accounts) {
			for (Farm farm : acc.getFarm()) {
				farmsDTO = new FarmsDTO();
				farmsDTO.setName(farm.getName());
				farmsDTO.setCity(farm.getCity());
				farmsDTO.setSizeInAre(farm.getSizeInAre().toString());
				farmsList.add(farmsDTO);
			}
		}
		return farmsList;
	}

}
