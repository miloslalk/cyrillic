package lalkovic.milos.cyrillic.Assignment.dto;

public class AccountDTO {

	private String type;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
