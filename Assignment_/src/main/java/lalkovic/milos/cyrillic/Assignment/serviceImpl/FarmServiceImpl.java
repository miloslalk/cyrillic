package lalkovic.milos.cyrillic.Assignment.serviceImpl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import lalkovic.milos.cyrillic.Assignment.entity.Farm;
import lalkovic.milos.cyrillic.Assignment.service.FarmService;

@Repository
public class FarmServiceImpl implements FarmService {

	@Autowired
	private EntityManager entityManager;


	@Override
	@Transactional
	public List<Farm> findAll() {
		Session session = entityManager.unwrap(Session.class);
		Query<Farm> theQuery = session.createQuery("from Farm", Farm.class);
		List<Farm> farms = theQuery.getResultList();
//		

		return farms;
	}

}
