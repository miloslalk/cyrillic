package lalkovic.milos.cyrillic.Assignment.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lalkovic.milos.cyrillic.Assignment.dto.AccountDTO;
import lalkovic.milos.cyrillic.Assignment.dto.FarmsDTO;
import lalkovic.milos.cyrillic.Assignment.service.AccountService;

@RestController
@RequestMapping("/api")
public class AccountController {
	
	@Autowired
	private AccountService accountService;

	@GetMapping("/accounts")
	public List<AccountDTO> findAllAccounts() {
		return accountService.findAllAccounts();
	}

	@GetMapping("/farms")
	public List<FarmsDTO> findAllFarms() {
		return accountService.findAllFarms();
	}
}
